package examen_isp_2021;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;

public class S2 extends JFrame {
    String txtfile="exam.txt";
    JTextArea textArea;
    JButton button;
    S2(){
        setTitle("S2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300,400);
        init();
        setVisible(true);

    }

    void init(){

        this.setLayout(null);


        textArea = new JTextArea();
        textArea.setBounds(50,150,200,20);
        add(textArea);
        button = new JButton("Submit text");
        button.setBounds(50,200,200,20);
        add(button);


        button.addActionListener(event -> {
            String str=textArea.getText();
            try {

                PrintWriter outF1 = new PrintWriter(new BufferedWriter(new FileWriter(txtfile)));
                outF1.println(str);
                outF1.close();
            } catch (IOException ioex) {

            }

        });

    }



    public static void main(String[] args) {
        S2 myframe = new S2();
    }
}